import './App.css'
import { GenerateKeyForm } from './components/GenerateKeyForm/GenerateKeyForm'


export default function App() {
  return (
    <div className='mx-auto'>
      <GenerateKeyForm />
    </div>
  )
}