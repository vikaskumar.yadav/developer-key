import { useEffect, useState, useRef } from 'react';




type SocketError = {
    type: 'connection_error' | 'generic_error' | 'authenticate.error',
    message: string;
};

type DeveloperKeyResponse = {
    developerKey: DeveloperKey
}

type DeveloperKey = {
    key: string,
    subscriptionId: string,
    accountId: string,
    createdTs: number
}


type SocketMessage = {
    type: 'hello' | 'authenticate' | 'authenticate.error' | 'authenticate.ok' | 'iam.developerKey.create.ok';
    response: string;
}

interface SocketHook {
    developerKey: DeveloperKey | null;
    sendMessage: (text: string) => void;
    error: SocketError | null;
}


const useSocket = (url: string): SocketHook => {
    const [developerKey, setdDeveloperKey] = useState<DeveloperKey | null>(null)
    const [error, setError] = useState<SocketError | null>(null);
    const webSocket = useRef<WebSocket | null>(null)

    const generateKey = () => {
        sendMessage(JSON.stringify({
            "type": "iam.developerKey.create"
        }));
    }

    const handleMessage = (event: MessageEvent<string>) => {
        const { type, response } = JSON.parse(event.data) as SocketMessage;
        if (type === 'authenticate.error') {
            setError({
                type,
                message: 'User name or password is incorrect'
            })
        } else if (type === 'authenticate.ok') {
            generateKey()
        } else if (type === 'iam.developerKey.create.ok') {
            const { developerKey } = response as unknown as DeveloperKeyResponse
            setdDeveloperKey(developerKey);
        }
    }

    useEffect(() => {
        webSocket.current = new WebSocket(url);
        webSocket.current.onopen = () => {
            console.log('Connected to the WebSocket server!');
        };

        webSocket.current.onmessage = (event) => {
            console.log('On Message -> ', event)
            handleMessage(event)
        };

        webSocket.current.onclose = () => {
            console.log('Connection to the WebSocket server closed');
        };

        webSocket.current.onerror = (event) => {
            console.error({ event })
        };


        return () => {
            webSocket.current?.close();
            setError(null);
            setdDeveloperKey(null)
        };
    }, [url]);

    const sendMessage = (message: string) => {
        setError(null)
        if (webSocket.current?.readyState === WebSocket.OPEN) {
            webSocket.current.send(message);
        } else {
            setError({ type: 'connection_error', message: 'WebSocket is not connected. Please reconnect.' });
        }
    };

    return { developerKey, sendMessage, error };
};

export default useSocket;
