# 👩‍💻 Developer Key 🗝

This repo let you create the developer key for dev environment
## Project Brief

🔍 Currently for creating the developer key we have to do connect to websocket through external app or through argos. This projcet let you do it with a single click and abstracting away all other details of connecting to websocket and sending different requests.

> Currently it only support the `DEV` environment. If you want to generate the key for other environment and you know the websocket url for taht environment then you can run the project locally and change the `DEV_SOCKET_URL` in `GenerateKeyForm` component. It will let you connect with that environment.

## Prerequisite

It required `Node version 20+`. The project is generated using vite and vite does not support older node version.

## Setup

### 🚀 Clone the repo 
```sh 
git clone git@gitlab.com:vikaskumar.yadav/developer-key.git
```

### 📁 Change directory to project

```sh
cd develoepr-key
```

### 💻 Install dependencies

```sh
yarn
# or
npm install
```
### 🏃‍♂️ Run project locally 

```sh
yarn dev
# or 
npm run dev
```
### 🌐 Go to the browser

Navigate to `http://localhost:5173/` to see teh app.

Feel free to connect to me `vikaskumar.yadav@pearson.com` if you have any questions or just raise the PR with any change you think that can improve this app.